import java.util.*;

public class JavaCollection {
    public static void main (String[] args) {
        System.out.println("\nExamples of Java Collections\n\n-LIST\n An ArrayList is different than an Array because it can be modified");

        List list = new ArrayList();
        list.add("Minecraft");
        list.add("Halo");
        list.add("Call of Duty");

        Scanner scanner = new Scanner(System.in);
        Integer c = 0;

        while(c < 1) {
            System.out.println("Enter a Video Game: ");
            String vgName = scanner.nextLine();

            if (vgName.length() > 0) {
                list.add(vgName);
                c++;
            } else {
                System.out.println("Please enter in a Video Game Name\n");
            }
        }



        for (Object str : list) {
            System.out.println((String) str);
        }

        System.out.println("\n\n-QUEUE\n A queue implements First IN First OUT rules, meaning that the elements that are entered first will be deleted first.\n");

        Queue<Integer> queue = new LinkedList<>();
        queue.add(4);
        queue.add(2);
        queue.add(1);
        queue.add(6);
        queue.add(8);
        System.out.println("The queue is: " + queue);
        int num1 = queue.remove();
        System.out.println("The element deleted from the head is: " + num1);
        System.out.println("The queue after deletion is: " + queue);
        int head = queue.peek();
        System.out.println("The head of this queue is: " + head);
        int size = queue.size();
        System.out.println("The size of this queue is: " + size);


        System.out.println("\n\n-SET\n A HashSet uses a hash table for storage and uses hashing, this means that each element has to be unique or it doesn't get added.");

        Set set = new HashSet();
        set.add("Brock");
        set.add("Garrett");
        set.add("Derek");
        set.add("Brock");

        for (Object str : set) {
            System.out.println((String) str);
        }


        System.out.println("\n\n-Tree\n A TreeMap is sorted in the ascending order and is has elements connected to keys\n");


        /* This is how to declare TreeMap */
        TreeMap<Integer, String> treeMap = new TreeMap<Integer, String>();

        /*Adding elements to TreeMap*/
        treeMap.put(4, "Badger");
        treeMap.put(2, "Jolley");
        treeMap.put(1, "Gobble");
        treeMap.put(3, "Ford");

        /* Display content using Iterator*/
        Set set1 = treeMap.entrySet();
        Iterator iterator = set1.iterator();
        while(iterator.hasNext()) {
            Map.Entry mentry = (Map.Entry)iterator.next();
            System.out.print("The key is: "+ mentry.getKey() + " and the value is: ");
            System.out.println(mentry.getValue());
        }
    }
}
