public class TimeKeeper implements Runnable{

    private String tName;
    private int time;
    private int duration;

    public TimeKeeper(String tName, int time, int duration) {
        this.tName = tName;
        this.time = time;
        this.duration = duration;
    }

    @Override
    public void run() {
        System.out.println(tName + " timer running for " + duration + " seconds");
        for (int i = 0; i < duration; i++) {
            time++;
            System.out.print("\rTime: " + time);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                System.err.println(e.toString());
            }
        }
        System.out.println("\n" + tName + " timer finished at " + time);
    }
}
