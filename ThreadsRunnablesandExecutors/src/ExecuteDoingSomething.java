import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;

public class ExecuteDoingSomething {

    public static void main(String[] args) {
        ExecutorService myService = Executors.newFixedThreadPool(3);

        DoingSomething ds1 = new DoingSomething("Brock", 20, 1000);
        DoingSomething ds2 = new DoingSomething("Garrett", 18, 800);
        DoingSomething ds3 = new DoingSomething("Derek", 16, 600);
        DoingSomething ds4 = new DoingSomething("Packer", 14, 400);
        DoingSomething ds5 = new DoingSomething("John", 12, 200);

        myService.execute(ds1);
        myService.execute(ds2);
        myService.execute(ds3);
        myService.execute(ds4);
        myService.execute(ds5);

        myService.shutdown();
    }
}
