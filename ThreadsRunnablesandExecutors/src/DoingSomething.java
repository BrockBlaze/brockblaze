import java.util.Random;

public class DoingSomething implements Runnable {

    private String name;
    private int number;
    private int sleep;
    private int rand;

    public DoingSomething(String name, int number, int sleep) {
        this.name = name;
        this.number = number;
        this.sleep = sleep;

        Random random = new Random();
        this.rand = random.nextInt(1000);
    }

    @Override
    public void run() {
        System.out.println("\nExecuting with these parameters: Name " + name + " Number = " + number + " Sleep = " + sleep + " Random Number = " + rand);
        for (int count = 1; count < rand; count++) {
            if (count % number == 0) {
                System.out.print(name + " is sleeping. \n");
                try {
                    Thread.sleep(sleep);
                } catch (InterruptedException e) {
                    System.err.println(e.toString());
                }
            }
        }
        System.out.println("\n" + name + " is done.");
    }
}
