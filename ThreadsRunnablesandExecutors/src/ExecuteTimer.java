import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecuteTimer {
    public static void main(String[] args) {
        ExecutorService myService = Executors.newFixedThreadPool(2);

        TimeKeeper timer1 = new TimeKeeper("First",0,10);
        TimeKeeper timer2 = new TimeKeeper("Second",0,5);
        TimeKeeper timer3 = new TimeKeeper("Third",0,2);

        myService.execute(timer1);
        myService.execute(timer2);
        myService.execute(timer3);

        myService.shutdown();
    }
}
