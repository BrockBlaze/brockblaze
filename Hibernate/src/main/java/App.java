import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;
import java.util.Scanner;

public class App {
    SessionFactory factory = null;
    Session session = null;

    private static App single_instance = null;
    private Object Book;

    private App()
    {
        factory = Hibernate.getSessionFactory();
    }

    public static App getInstance()
    {
        if (single_instance == null) {
            single_instance = new App();
        }

        return single_instance;
    }

    public List<Book> getBooks() {
        try {
            session = factory.openSession();
            session .getTransaction().begin();
            String sql = "from Book";
            List<Book> b = (List<Book>)session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return b;
        } catch (Exception e) {
            e.printStackTrace();

            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

   public Book getBook(int book_id) {
        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from Book where id = " + Integer.toString(book_id);
            Book b = (Book)session.createQuery(sql).getSingleResult();
            session.getTransaction().commit();
            return b;
        } catch (Exception e) {
            e.printStackTrace();

            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

    public static void main(String[] args) {

        App test = App.getInstance();
        Integer selection;
        Integer exit = 0;

        System.out.println("All of the books in the Database:\n");

        List<Book> b = test.getBooks();
        for (Book i : b) {
            System.out.println(i);
        }

        Scanner s = new Scanner(System.in);

        while (exit == 0) {
            System.out.println("\n\nWhat book do you want to select");
            try {
                selection = s.nextInt();
                if (selection == 1 || selection == 2 || selection == 3) {
                    System.out.println("\nYou selected the book with the Id of: " + selection);
                    System.out.println(test.getBook(selection));
                    exit++;
                } else {
                    System.out.println("\nPlease select an available book number");
                }
            } catch (Exception e) {
                System.err.println(e.toString());
                selection = 0;
            }
        }
    }
}
