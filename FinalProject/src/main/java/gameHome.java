import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "gameHome", urlPatterns = {"/gameHome"})
public class gameHome extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Player p = Test.pData;

        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>blazztech</title>\n" +
                "    <link rel=\"stylesheet\" href=\"./css/main.css\">\n" +
                "</head>\n" +
                "<body>\n" +
                "<h1>Atrium</h1>\n" +
                "<nav><a href=\"./\">home</a> <a href=\"./test.html\"> test</a> <a href=\"./about.html\"> about</a></nav>\n" +
                "<main>\n" +
                "    <h2>Welcome to Atrium</h2>");

        out.println("<div class=\"stats\">");

        out.println("<p>Name: " + p.getName() + "</p>");
        if(p.getType() == 1)
        {
            out.println("<p>Type: Human</p>");
        }
        if(p.getType() == 2)
        {
            out.println("<p>Type: Elf</p>");
        }
        if(p.getType() == 3)
        {
            out.println("<p>Type: Wizard</p>");
        }
        out.println("<p>Level: " + p.getLevel() + "</p>");
        out.println("<p>Health: " + p.getHealth() + "</p>");
        out.println("<p>Coins: " + p.getCoins() + "</p>");
        out.println("<p>Weapon: " + p.getWeapon() + "</p>");
        out.println("<p>Armor: " + p.getArmor() + "</p>");

        out.println("</div>");

        out.println("<form action=\"Fight\" method=\"post\">\n" +
                "        <p><input type=\"submit\" value=\"Fight\" /></p>\n" +
                "    </form>\n" +
                "    <p>Go to the Store</p>\n" +
                "    <form action=\"store.html\" method=\"post\">\n" +
                "        <p><input type=\"submit\" value=\"Store\" /></p>\n" +
                "    </form>\n" +
                "    <p>Go Home</p>\n" +
                "    <form action=\"home.html\" method=\"post\">\n" +
                "        <p><input type=\"submit\" value=\"Home\" /></p>\n" +
                "    </form>");
        out.println("</main>\n" +
                "</body>\n" +
                "<footer>\n" +
                "    &copy;<span id=\"year\"></span>\n" +
                "    <a href=\"https://brockblaze.github.io/\">blazztech projects | Brock F. Blazzard</a>\n" +
                "</footer>\n" +
                "<script>\n" +
                "    const d = new Date();\n" +
                "    document.getElementById(\"year\").innerHTML = d.getFullYear();\n" +
                "</script>\n" +
                "</html>");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
