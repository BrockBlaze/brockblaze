import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "buyWeapon", urlPatterns = {"/buyWeapon"})
public class buyWeapon extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Player p = Test.pData;

        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>blazztech</title>\n" +
                "    <link rel=\"stylesheet\" href=\"./css/main.css\">\n" +
                "</head>\n" +
                "<body>\n" +
                "<h1>Atrium</h1>\n" +
                "<nav><a href=\"./\">home</a> <a href=\"./test.html\"> test</a> <a href=\"./about.html\"> about</a></nav>\n" +
                "<main>\n");

        if (p.getType() == 1){
            out.println("<h2>Welcome Human to the store</h2>");

            if (p.getWeapon().equals("Wood Sword")){
                out.println("<p>Would you like to by a Iron Sword for 20 coins</p>");
                out.println("<form action=\"bought\" method=\"post\">\n" +
                        "       <input type=\"hidden\" name=\"item\" value=\"Iron Sword\" />" +
                        "       <input type=\"hidden\" name=\"cost\" value=\"20\" />"+
                        "       <p><input type=\"submit\" name=\"item\" value=\"Buy\" /></p>\n" +
                        "    </form>");
            }
            if (p.getWeapon().equals("Iron Sword")){
                out.println("<p>Would you like to by a Steel Sword for 30 coins</p>");
                out.println("<form action=\"bought\" method=\"post\">\n" +
                        "       <input type=\"hidden\" name=\"item\" value=\"Steel Sword\" />" +
                        "       <input type=\"hidden\" name=\"cost\" value=\"30\" />"+
                        "       <p><input type=\"submit\" name=\"item\" value=\"Buy\" /></p>\n" +
                        "    </form>");
            }
            if (p.getWeapon().equals("Steel Sword")){
                out.println("<p>You already own the best Sword</p>");
            }

        }

        if (p.getType() == 2){
            out.println("<h2>Welcome Elf to the store</h2>");

            if (p.getWeapon().equals("Wood Bow")){
                out.println("<p>Would you like to by a Iron Bow for 20 coins</p>");
                out.println("<form action=\"bought\" method=\"post\">\n" +
                        "       <input type=\"hidden\" name=\"item\" value=\"Iron Bow\" />" +
                        "       <input type=\"hidden\" name=\"cost\" value=\"20\" />"+
                        "       <p><input type=\"submit\" name=\"item\" value=\"Buy\" /></p>\n" +
                        "    </form>");
            }
            if (p.getWeapon().equals("Iron Bow")){
                out.println("<p>Would you like to by a Steel Bow for 30 coins</p>");
                out.println("<form action=\"bought\" method=\"post\">\n" +
                        "       <input type=\"hidden\" name=\"item\" value=\"Steel Bow\" />" +
                        "       <input type=\"hidden\" name=\"cost\" value=\"30\" />"+
                        "       <p><input type=\"submit\" name=\"item\" value=\"Buy\" /></p>\n" +
                        "    </form>");
            }
            if (p.getWeapon().equals("Steel Bow")){
                out.println("<p>You already own the best Bow</p>");
            }

        }

        if (p.getType() == 3){
            out.println("<h2>Welcome Wizard to the store</h2>");

            if (p.getWeapon().equals("Wood Staff")){
                out.println("<p>Would you like to by a Iron Staff for 20 coins</p>");
                out.println("<form action=\"bought\" method=\"post\">\n" +
                        "       <input type=\"hidden\" name=\"item\" value=\"Iron Staff\" />" +
                        "       <input type=\"hidden\" name=\"cost\" value=\"20\" />"+
                        "       <p><input type=\"submit\" name=\"item\" value=\"Buy\" /></p>\n" +
                        "    </form>");
            }
            if (p.getWeapon().equals("Iron Staff")){
                out.println("<p>Would you like to by a Steel Staff for 30 coins</p>");
                out.println("<form action=\"bought\" method=\"post\">\n" +
                        "       <input type=\"hidden\" name=\"item\" value=\"Steel Staff\" />" +
                        "       <input type=\"hidden\" name=\"cost\" value=\"30\" />"+
                        "       <p><input type=\"submit\" name=\"item\" value=\"Buy\" /></p>\n" +
                        "    </form>");
            }
            if (p.getWeapon().equals("Steel Staff")){
                out.println("<p>You already own the best Staff</p>");
            }

        }

        out.println("<p>Go Home</p>\n" +
                "    <form action=\"gameHome\" method=\"post\">\n" +
                "        <p><input type=\"submit\" value=\"Home\" /></p>\n" +
                "    </form>");

        out.println("</main>\n" +
                "</body>\n" +
                "<footer>\n" +
                "    &copy;<span id=\"year\"></span>\n" +
                "    <a href=\"https://brockblaze.github.io/\">blazztech projects | Brock F. Blazzard</a>\n" +
                "</footer>\n" +
                "<script>\n" +
                "    const d = new Date();\n" +
                "    document.getElementById(\"year\").innerHTML = d.getFullYear();\n" +
                "</script>\n" +
                "</html>");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
