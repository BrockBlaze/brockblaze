import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "hTests", urlPatterns = {"/hTests"})
public class hTests extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        hibernateMethods test = hibernateMethods.getInstance();
        Player p = Test.pData;


        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<html><head><title>blazztech</title><link rel=\"stylesheet\" href=\"./css/main.css\"></head><body><main>");
        out.println("<p>Updated</p>");
        out.println(p);
        out.println("</main>");
        out.println("</body></html>");
        test.deletePlayer(p);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
