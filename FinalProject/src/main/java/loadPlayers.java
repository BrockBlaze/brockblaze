import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet(name = "loadPlayers", urlPatterns = {"/loadPlayers"})
public class loadPlayers extends HttpServlet {
    SessionFactory factory = null;
    Session session = null;

    private static loadPlayers single_instance = null;
    private Object Player;

    private loadPlayers()
    {
        factory = Hibernate.getSessionFactory();
    }

    public static loadPlayers getInstance()
    {
        if (single_instance == null) {
            single_instance = new loadPlayers();
        }

        return single_instance;
    }

    public List<Player> getPlayers() {
        try {
            session = factory.openSession();
            session .getTransaction().begin();
            String sql = "from Player";
            List<Player> p = (List<Player>)session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return p;
        } catch (Exception e) {
            e.printStackTrace();

            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<html><head><link rel=\"stylesheet\" href=\"./css/main.css\"></head><body>");
        out.println("<main>");
        loadPlayers test = loadPlayers.getInstance();

        List<Player> p = test.getPlayers();
        for (Player i : p) {
            out.println("<p>" + i + "</p>");
        }

        out.println("</main>");
        out.println("</body></html>");

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("No numbers entered!");

    }
}
