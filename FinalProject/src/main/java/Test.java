import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;
import java.util.Scanner;

public class Test {
    SessionFactory factory = null;
    Session session = null;

    private static Test single_instance = null;
    private Object Player;
    public static String playerName;
    public static Player pData;

    private Test()
    {
        factory = Hibernate.getSessionFactory();
    }

    public static Test getInstance()
    {
        if (single_instance == null) {
            single_instance = new Test();
        }

        return single_instance;
    }


    public Player getPlayer(int id) {
        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from Player where id = " + Integer.toString(id);
            Player p = (Player)session.createQuery(sql).getSingleResult();
            session.getTransaction().commit();
            playerName = p.getName();
            pData = p;
            return p;
        } catch (Exception e) {
            e.printStackTrace();

            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

    public Player deletePlayer(int id) {
        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from Player where id = " + Integer.toString(id);
            Player p = (Player)session.createQuery(sql).getSingleResult();
            session.getTransaction().commit();
            session.delete(p);
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

    public Player setPlayer(String name, Integer type, Integer level, Integer health, Integer coins, String weapon, String armor) {
        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "Insert Into Player " + "Values (" + "'"+name+"',"+"'"+ type+"',"+"'"+level+"',"+"'"+health+"',"+"'"+coins+"',"+"'"+weapon+"',"+"'"+armor+"')";
            Player p = (Player)session.createQuery(sql).getSingleResult();
            session.getTransaction().commit();
            return p;
        } catch (Exception e) {
            session.getTransaction().rollback();
            return null;
        }finally {
            session.close();
        }




    }

    public List<Player> getPlayers() {
        try {
            session = factory.openSession();
            session .getTransaction().begin();
            String sql = "from Player";
            List<Player> p = (List<Player>)session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return p;
        } catch (Exception e) {
            e.printStackTrace();

            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

    public static void main(String[] args) {

        Test test = Test.getInstance();
        Integer selection;
        Integer exit = 0;


        Scanner s = new Scanner(System.in);

        while (exit == 0) {
            System.out.println("\n\nWhat Player do you want to select");
            try {
                selection = s.nextInt();
                if (selection == 1 || selection == 2 || selection == 3) {
                    System.out.println("\nYou selected the Player with the Id of: " + selection);
                    System.out.println(test.getPlayer(selection));
                    exit++;
                } else {
                    System.out.println("\nPlease select an available Player id number");
                }
            } catch (Exception e) {
                System.err.println(e.toString());
                selection = 0;
            }
        }
    }
}