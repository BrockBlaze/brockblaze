

public class sleepThread implements Runnable{

    private double time;
    public String done = "Sleeping";

    public sleepThread(double time){
        this.time = time;

    }

    @Override
    public void run() {
        for (int i = 0; i < time; i++) {
            time++;
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                System.err.println(e.toString());
            }
        }
        done = "Done Sleeping";
    }
}
