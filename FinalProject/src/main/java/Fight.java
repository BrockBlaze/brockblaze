import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;

@WebServlet(name = "Fight", urlPatterns = {"/Fight"})
public class Fight extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        hibernateMethods test = hibernateMethods.getInstance();
        Player p = Test.pData;

        Random rand = new Random();
        int c = rand.nextInt(10);

        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>blazztech</title>\n" +
                "    <link rel=\"stylesheet\" href=\"./css/main.css\">\n" +
                "</head>\n" +
                "<body>\n" +
                "<h1>Atrium</h1>\n" +
                "<nav><a href=\"./\">home</a> <a href=\"./test.html\"> test</a> <a href=\"./about.html\"> about</a></nav>\n" +
                "<main>\n");

        out.println("<main><h2>You fought a Wolf</h2>");
        Integer x = p.getHealth();
        x = x - c;

        Integer a = p.getCoins();
        a = a + c;

        if (p.getHealth() > 0) {
            p.setHealth(x);
            p.setCoins(a);
            out.println("<p>You Lost " + c + " Health</p>");
            out.println("<p>You Gain " + c + " Coins</p>");
        }else {
            out.println("<p>And you Died... Player Deleted</p>");
            test.deletePlayer(p);
        }

        if (c == 1){
            Integer l = p.getLevel() + 1;
            p.setLevel(l);
        }

        Test.pData.setHealth(p.getHealth());
        Test.pData.setCoins(p.getCoins());
        Test.pData.setLevel(p.getLevel());

        out.println("<p>Go Home</p>\n" +
                "    <form action=\"gameHome\" method=\"post\">\n" +
                "        <p><input type=\"submit\" value=\"Home\" /></p>\n" +
                "    </form>");

        out.println("</main>\n" +
                "</body>\n" +
                "<footer>\n" +
                "    &copy;<span id=\"year\"></span>\n" +
                "    <a href=\"https://brockblaze.github.io/\">blazztech projects | Brock F. Blazzard</a>\n" +
                "</footer>\n" +
                "<script>\n" +
                "    const d = new Date();\n" +
                "    document.getElementById(\"year\").innerHTML = d.getFullYear();\n" +
                "</script>\n" +
                "</html>");

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
