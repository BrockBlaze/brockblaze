import javax.persistence.*;

@Entity
@Table(name = "players")
public class Player {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "type")
    private int type;

    @Column(name = "level")
    private int level;

    @Column(name = "health")
    private int health;

    @Column(name = "coins")
    private int coins;

    @Column(name = "weapon")
    private String weapon;

    @Column(name = "armor")
    private String armor;

    public int getId() { return id; }
    public void setId(int id) { this.id = id; }

    public String getName() { return name; }
    public void setName(String name) { this.name = name; }

    public int getType() { return type; }
    public void setType(int type) { this.type = type; }

    public int getLevel() { return level; }
    public void setLevel(int level) { this.level = level; }

    public int getHealth() { return health; }
    public void setHealth(int health) { this.health = health; }

    public int getCoins() { return coins; }
    public void setCoins(int coins) { this.coins = coins; }

    public String getWeapon() { return weapon; }
    public void setWeapon(String weapon) { this.weapon = weapon; }

    public String getArmor() { return armor; }
    public void setArmor(String armor) { this.armor = armor; }

    @Override
    public String toString() {
        return "Player " + id +
                " name: " + name +
                " level: " + level + " ";
    }
}
