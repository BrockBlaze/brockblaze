import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class hibernateMethods {
    SessionFactory factory = null;
    Session session = null;

    private static hibernateMethods single_instance = null;
    private Object Player;

    private hibernateMethods()
    {
        factory = Hibernate.getSessionFactory();
    }

    public static hibernateMethods getInstance()
    {
        if (single_instance == null) {
            single_instance = new hibernateMethods();
        }

        return single_instance;
    }

    public Player updatePlayer(Player p){

        try {
            p.setId(Test.pData.getId());
            p.setName(Test.pData.getName());
            p.setType(Test.pData.getType());
            p.setLevel(Test.pData.getLevel());
            p.setHealth(Test.pData.getHealth());
            p.setCoins(Test.pData.getCoins());
            p.setWeapon(Test.pData.getWeapon());
            p.setArmor(Test.pData.getArmor());

            session = factory.openSession();
            session.getTransaction().begin();
            session.update(p);
            session.getTransaction().commit();
            return p;
        }catch (Exception e) {
            e.printStackTrace();

            session.getTransaction().rollback();
            return null;
        }finally {
            session.close();
        }
    }

    public Player deletePlayer(Player p){

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            session.delete(p);
            session.getTransaction().commit();
            return p;
        }catch (Exception e) {
            e.printStackTrace();

            session.getTransaction().rollback();
            return null;
        }finally {
            session.close();
        }
    }

    public Player createPlayer(Player p){

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            session.save(p);
            session.getTransaction().commit();
            Test.pData = p;
            return p;
        }catch (Exception e) {
            e.printStackTrace();

            session.getTransaction().rollback();
            return null;
        }finally {
            session.close();
        }
    }
}
