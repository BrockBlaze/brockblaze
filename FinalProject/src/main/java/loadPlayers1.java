import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;


@WebServlet(name = "loadPlayers1", urlPatterns = {"/loadPlayers1"})
public class loadPlayers1 extends HttpServlet {

    public Integer x = 0;
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Test test = Test.getInstance();
        List<Player> b = test.getPlayers();

        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<html><head><title>blazztech</title><link rel=\"stylesheet\" href=\"./css/main.css\"></head><body><h1>Final Project</h1>\n" +
                "<nav><a href=\"./\">home</a> <a href=\"./test.html\"> test</a> <a href=\"./about.html\"> about</a></nav>");
        out.println("<main><h2>Welcome to Atrium</h2><p>Start New Game</p>\n" +
                "     <form action=\"StartNew.html\" method=\"post\">\n" +
                "      <p><input type=\"submit\" value=\"Start\" /></p>\n" +
                "     </form>\n" +
                "    <p>Loaded Players</p>" +
                "<form action=\"loadPlayerSel\" method=\"post\">");

        for (Player i : b) {
            x = i.getId();
            out.println("<p>" + i + " <input type=\"submit\" name=\"selection\" value=\"" + x + "\">" +
                    "</p>");
        }

        out.println("</form></main></body>");
        out.println("<footer>&copy;<span id=\"year\"></span><a href=\"https://brockblaze.github.io/\">blazztech projects | Brock F. Blazzard</a></footer>" +
                "<script>\n" +
                "    const d = new Date();\n" +
                "    document.getElementById(\"year\").innerHTML = d.getFullYear();\n" +
                "</script>" +
                "</html>");

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("No numbers entered!");

    }
}
