import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.Random;

@WebServlet(name = "Sleep", urlPatterns = {"/Sleep"})
public class Sleep extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ExecutorService myService = Executors.newFixedThreadPool(1);

        Player p = Test.pData;

        Random rand = new Random();

        int r = rand.nextInt(10);

        sleepThread s = new sleepThread(r);
        myService.execute(s);

        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<html><head><title>blazztech</title><link rel=\"stylesheet\" href=\"./css/main.css\"></head><body><h1>Final Project</h1>\n" +
                "<nav><a href=\"./\">home</a> <a href=\"./test.html\"> test</a> <a href=\"./about.html\"> about</a></nav>");
        out.println("<main><h2>You are "  + s.done);
        Integer x = p.getHealth();
        x = x + r;

        if (p.getHealth() < 100) {
            p.setHealth(x);
            out.println("<p>You gained " + r + "</p>");
        }else {
            p.setHealth(100);
        }

        Test.pData.setHealth(p.getHealth());
        out.println("<form action=\"home.html\" method=\"post\">\n" +
                "        <p><input type=\"submit\" value=\"Home\" /></p>" +
                "   </form></main></body>\n" +
                "<footer>\n" +
                "    &copy;<span id=\"year\"></span>\n" +
                "    <a href=\"https://brockblaze.github.io/\">blazztech projects | Brock F. Blazzard</a>\n" +
                "</footer>\n" +
                "<script>\n" +
                "    const d = new Date();\n" +
                "    document.getElementById(\"year\").innerHTML = d.getFullYear();\n" +
                "</script></html>");
        myService.shutdown();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
