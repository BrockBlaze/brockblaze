import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "newPlayer", urlPatterns = {"/newPlayer"})
public class newPlayer extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        hibernateMethods test = hibernateMethods.getInstance();

        String name = request.getParameter("name");
        String t = request.getParameter("type");
        Integer type = Integer.parseInt(t);

        Player p = new Player();
        p.setName(name);
        p.setType(type);
        p.setLevel(1);
        p.setHealth(100);
        p.setCoins(10);

        if (t.equals("1"))
        {
            p.setWeapon("Wood Sword");
            p.setArmor("Iron Armor");
        }
        if (t.equals("2"))
        {
            p.setWeapon("Wood Dagger");
            p.setArmor("Iron Armor");
        }
        if (t.equals("3"))
        {
            p.setWeapon("Wood Staff");
            p.setArmor("Brown Robes");
        }


        test.createPlayer(p);

        PrintWriter out = response.getWriter();
        response.setContentType("text/html");

        out.println("<html><head><title>blazztech</title><link rel=\"stylesheet\" href=\"./css/main.css\"></head><body><h1>Final Project</h1>\n" +
                "<nav><a href=\"./\">home</a> <a href=\"./test.html\"> test</a> <a href=\"./about.html\"> about</a></nav>");
        out.println("<main>" +
                "<h2>Welcome " + name + "</h2>");
        out.println("<form action=\"gameHome\" method=\"post\">\n" +
                "        <p><input type=\"submit\" value=\"Play\" /></p>" +
                "   </form>\n" +
                "   <form action=\"Delete\" method=\"post\">\n" +
                "        <p><input type=\"submit\" value=\"Delete\" /></p>" +
                "   </form></main></body>");
        out.println("<footer>&copy;<span id=\"year\"></span><a href=\"https://brockblaze.github.io/\">blazztech projects | Brock F. Blazzard</a></footer>" +
                "<script>\n" +
                "    const d = new Date();\n" +
                "    document.getElementById(\"year\").innerHTML = d.getFullYear();\n" +
                "</script>" +
                "</html>");

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
