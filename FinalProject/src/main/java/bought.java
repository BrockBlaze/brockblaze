import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "bought", urlPatterns = {"/bought"})
public class bought extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Player p = Test.pData;

        String item = request.getParameter("item");
        String c = request.getParameter("cost");
        Integer cost = Integer.parseInt(c);

        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<!DOCTYPE html>\n" +
                "<html lang=\"en\">\n" +
                "<head>\n" +
                "    <meta charset=\"UTF-8\">\n" +
                "    <title>blazztech</title>\n" +
                "    <link rel=\"stylesheet\" href=\"./css/main.css\">\n" +
                "</head>\n" +
                "<body>\n" +
                "<h1>Atrium</h1>\n" +
                "<nav><a href=\"./\">home</a> <a href=\"./test.html\"> test</a> <a href=\"./about.html\"> about</a></nav>\n" +
                "<main>\n");


        if (p.getCoins() >= cost){
            out.println("<h2>" + item + " bought</h2>");
            out.println("<p>" + cost + " coins removed</p>");
            p.setCoins(p.getCoins() - cost);
            Test.pData.setCoins(p.getCoins());
            if(item.equals("Iron Sword") || item.equals("Steel Sword") || item.equals("Iron Dagger") || item.equals("Steel Dagger") || item.equals("Iron Staff") || item.equals("Steel Staff")){
                Test.pData.setWeapon(item);
            }
            if(item.equals("Steel Armor") || item.equals("Blue Robes") || item.equals("White Robes")){
                Test.pData.setArmor(item);
            }
        }
        else {
            out.println("<h2>You can't afford this</h2>");
        }




        out.println("<p>Go Home</p>\n" +
                "    <form action=\"gameHome\" method=\"post\">\n" +
                "        <p><input type=\"submit\" value=\"Home\" /></p>\n" +
                "    </form>");

        out.println("</main>\n" +
                "</body>\n" +
                "<footer>\n" +
                "    &copy;<span id=\"year\"></span>\n" +
                "    <a href=\"https://brockblaze.github.io/\">blazztech projects | Brock F. Blazzard</a>\n" +
                "</footer>\n" +
                "<script>\n" +
                "    const d = new Date();\n" +
                "    document.getElementById(\"year\").innerHTML = d.getFullYear();\n" +
                "</script>\n" +
                "</html>");

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
