import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "loadPlayerSel", urlPatterns = {"/loadPlayerSel"})
public class loadPlayerSel extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String selection = request.getParameter("selection");
        Integer sel = Integer.parseInt(selection);

        Test test = Test.getInstance();
        Player p = test.getPlayer(sel);


        PrintWriter out = response.getWriter();
        response.setContentType("text/html");

        out.println("<html><head><title>blazztech</title><link rel=\"stylesheet\" href=\"./css/main.css\"></head><body><h1>Final Project</h1>\n" +
                "<nav><a href=\"./\">home</a> <a href=\"./test.html\"> test</a> <a href=\"./about.html\"> about</a></nav>");
        out.println("<main><h2>Welcome " + Test.playerName + " to Atrium</h2>");
        out.println("<form action=\"gameHome\" method=\"post\">\n" +
                "        <p><input type=\"submit\" value=\"Play\" /></p>" +
                "   </form>\n" +
                "   <form action=\"Delete\" method=\"post\">\n" +
                "        <p><input type=\"submit\" value=\"Delete\" /></p>" +
                "   </form></main></body>\n" +
                "<footer>\n" +
                "    &copy;<span id=\"year\"></span>\n" +
                "    <a href=\"https://brockblaze.github.io/\">blazztech projects | Brock F. Blazzard</a>\n" +
                "</footer>\n" +
                "<script>\n" +
                "    const d = new Date();\n" +
                "    document.getElementById(\"year\").innerHTML = d.getFullYear();\n" +
                "</script></html>");


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
