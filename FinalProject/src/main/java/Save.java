import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "Save", urlPatterns = {"/Save"})
public class Save extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        hibernateMethods test = hibernateMethods.getInstance();
        Player p = Test.pData;

        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<html><head><title>blazztech</title><link rel=\"stylesheet\" href=\"./css/main.css\"></head><body><h1>Final Project</h1>\n" +
                "<nav><a href=\"./\">home</a> <a href=\"./test.html\"> test</a> <a href=\"./about.html\"> about</a></nav>");
        out.println("<main><h2>Updated " + p.getName() + "'s Game</h2>");
        out.println("<form action=\"home.html\" method=\"post\">\n" +
                "        <p><input type=\"submit\" value=\"Home\" /></p>" +
                "    </form></main></body>\n" +
                "<footer>\n" +
                "    &copy;<span id=\"year\"></span>\n" +
                "    <a href=\"https://brockblaze.github.io/\">blazztech projects | Brock F. Blazzard</a>\n" +
                "</footer>\n" +
                "<script>\n" +
                "    const d = new Date();\n" +
                "    document.getElementById(\"year\").innerHTML = d.getFullYear();\n" +
                "</script></html>");
        test.updatePlayer(p);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
