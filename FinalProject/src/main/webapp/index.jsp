<%--
  Created by IntelliJ IDEA.
  User: brock
  Date: 12/5/2020
  Time: 7:38 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>blazztech</title>
    <link rel="stylesheet" href="./css/main.css">
</head>
<body>
    <h1>Final Project</h1>
    <nav><a href="./">home</a> <a href="./test.html"> test</a> <a href="./about.html"> about</a></nav>
    <main>
        <h2>Welcome to Atrium</h2>
        <p>Start New Game</p>
        <form action="StartNew.html" method="post">
            <p><input type="submit" value="Start" /></p>
        </form>
        <p>Load Players</p>
        <form action="loadPlayers1" method="post">
            <p><input type="submit" value="Load" /></p>
        </form>
    </main>
</body>
<footer>
    &copy;<span id="year"></span>
    <a href="https://brockblaze.github.io/">blazztech projects | Brock F. Blazzard</a>
</footer>
<script>
    const d = new Date();
    document.getElementById("year").innerHTML = d.getFullYear();
</script>
</html>
