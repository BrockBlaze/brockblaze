import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class JSONconverter {

    public static Player JSONToPlayer(String s) throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        Player player = null;

        try {
            player = mapper.readValue(s, Player.class);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return player;
    }

    public static void main(String[] args) throws IOException {
        String json = HTTPconnection.getHttpContent("https://brockblaze.github.io/CIT360/json/player.json");
        System.out.println(json);

        Player player1 = JSONconverter.JSONToPlayer(json);
        System.out.println(player1);
    }
}
