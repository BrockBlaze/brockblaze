import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;

public class JSON {

    public  static String customerToJSON(Customer customer) {

        ObjectMapper mapper = new ObjectMapper();
        String s = "";

        try {
            s = mapper.writeValueAsString(customer);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return s;
    }

    public static Customer JSONToCustomer(String s) throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        Customer customer = null;

        try {
            customer = mapper.readValue(s, Customer.class);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        }

        return customer;
    }

    public static void main(String[] args) throws IOException {

        Customer cust = new Customer();
        cust.setName("Brock");
        cust.setPhone(29192);

        String json = JSON.customerToJSON(cust);
        System.out.println(json);

        Customer cust2 = JSON.JSONToCustomer(json);
        System.out.println(cust2);
    }

}
