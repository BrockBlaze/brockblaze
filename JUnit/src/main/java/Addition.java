public class Addition {
    public static void main(String[] args) {
        System.out.println("\nJUnit Test on the Method add\n-------------------------\n");

        System.out.println(add(2,2));
    }
    public static int add(int x, int y) {
        return x+y;
    }
}
