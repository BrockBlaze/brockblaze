import org.junit.*;
import static org.junit.Assert.*;

public class AdditionTest {

    @Test
    public void testAdditionEquals() {
        Addition newAdd = new Addition();
        int x = newAdd.add(1, 3);
        assertEquals(4, x);
    }

    @Test
    public void testAdditionTrue() {
        Addition newAdd = new Addition();
        int x = newAdd.add(2, 2);
        assertTrue(x == 4);
    }

    @Test
    public void testAdditionFalse() {
        Addition newAdd = new Addition();
        int x = newAdd.add(2, 2);
        assertFalse(x == 5);
    }

    @Test
    public void testAdditionNotNull() {
        Addition newAdd = new Addition();
        int x = newAdd.add(2, 2);
        assertNotNull(x);
    }


    @Test
    public void testAdditionSame() {
        Addition newAdd = new Addition();
        int x = newAdd.add(2, 2);
        int y = newAdd.add(1, 3);
        assertSame(x,y);
    }

    @Test
    public void testAdditionNotSame() {
        Addition newAdd = new Addition();
        int x = newAdd.add(2, 2);
        int y = newAdd.add(2, 3);
        assertNotSame(x,y);
    }

    @Test
    public void testAdditionArrayEquals() {
        Addition newAdd = new Addition();
        int [] x = new int[] {newAdd.add(2,2)};
        int [] y = new int[] {newAdd.add(2,2)};
        assertArrayEquals(x,y);
    }

    /*@Test
    public void testAddition8() {
        Addition newAdd = new Addition();
        int x = newAdd.add(, 2);
        assertNull(x);
    }*/

}
