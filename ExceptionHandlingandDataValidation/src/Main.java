import java.util.Scanner;

public class Main {
    public  static int num1,num2, choice ;
    public static void main (String[] args) {
        System.out.println("\nExamples of Exception Handling and Data Validation\n\n");

        Scanner sc = new Scanner(System.in);
        System.out.println("Please enter in a Number");
        String snum1 = sc.nextLine();
        checkIfNumber(snum1);
        num1 = Integer.parseInt(snum1);
        System.out.println("Please enter in another Number");
        String snum2 = sc.nextLine();
        checkIfNumber(snum2);
        num2 = Integer.parseInt(snum2);

        menu();
    }

    public static void menu()
    {
        Scanner c = new Scanner(System.in);

        System.out.println("What do you want to do\n 1. Enter new Numbers \n 2. Divide Numbers \n 3. Quit");
        String tchoice = c.nextLine();
        checkMenu(tchoice);
        choice = Integer.parseInt(tchoice);

        if (choice == 1)
        {
            choice = 0;
            enterNumbers();
        }
        if (choice == 2)
        {
            choice = 0;
            divideNumbers(num1, num2);
        }
        if (choice == 3)
        {
            choice = 0;
            System.out.println("GoodBye");
            System.exit(0);
        }
        c.close();
    }


    public static void checkIfNumber(String x)
    {
        try {
            int temp = Integer.parseInt(x);
        }
        catch (Exception e)
        {
            System.out.println("Please enter a number");
            menu();
        }
    }

    public static  void checkMenu(String x)
    {
        try {
            int temp = Integer.parseInt(x);
        }
        catch (Exception e)
        {
           System.out.println("Please enter in a valid number");
           menu();
        }
    }


    public static void enterNumbers()
    {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter a number: ");
        String numS1 = scanner.nextLine();
        checkIfNumber(numS1);
        int tnum1 = Integer.parseInt(numS1);


        System.out.println("Enter a second number: ");
        String numS2 = scanner.nextLine();
        checkIfNumber(numS2);
        int tnum2 = Integer.parseInt(numS2);
        checkNumber(tnum1, tnum2);
        menu();

        scanner.close();
    }


    public static void checkNumber(int x, int y)
    {

        if (y < 1)
        {
            System.out.println("Warning.... You enter a number less then 0!");
            num1 = x;
            num2 = y;
            System.out.println("Number 1 is --- " + x);
            System.out.println("Number 2 is --- " + y + "\n\n");
            menu();
        }
        else
        {
            num1 = x;
            num2 = y;
            System.out.println("Number 1 is --- " + x);
            System.out.println("Number 2 is --- " + y + "\n\n");
        }
    }


    static void divideNumbers(int x, int y)
    {
        try {
            int d = x/y;
            System.out.println("Divided number is " + d + "\n\n");
        }
        catch (ArithmeticException e) {
            System.out.println("You can't divide by 0, Please enter a number greater then 0");
        }
        menu();
    }
}
