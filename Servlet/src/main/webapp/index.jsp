<%--
  Created by IntelliJ IDEA.
  User: brock
  Date: 10/28/2020
  Time: 5:45 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Home</title>
</head>
<style>
    body {background-color: slategray; margin: auto; width: 800px; padding: 20px}
</style>
<body>
    <h1>Hello Admin</h1>
    <p>Click <a href="${pageContext.request.contextPath}/login.html">here</a> to go to the login page</p>
    <p>Click <a href="${pageContext.request.contextPath}/numbers.html">here</a> to go Numbers App</p>
    <p>To start the java servlet click <a href="${pageContext.request.contextPath}/servlet">here</a></p>
</body>
</html>
