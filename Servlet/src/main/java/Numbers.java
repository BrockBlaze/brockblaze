import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "Numbers", urlPatterns = {"/Numbers"})
public class Numbers extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        out.println("<html><head></head><style>\n" +
                "    body {background-color: slategray; margin: auto; width: 800px; padding: 20px}\n" +
                "</style><body>");
        String number = request.getParameter("number");
        int tempNumber = Integer.parseInt(number);
        int doubleNumber = tempNumber * tempNumber;

        out.println("<h1>Numbers App</h1>");
        out.println("<p>Your number is: " + number + "</p>");
        out.println("<p>Your number squared is: " + doubleNumber + "</p>");
        out.println("</body></html>");

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("No numbers entered!");

    }
}
